import 'dart:io';

import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  static Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    }

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String pathStr = documentsDirectory.path;
    String path = "$pathStr/gestao_clinica.db";
    print(path);
    return await openDatabase(
         path, 
         version: 1, 
         onOpen: (db) {}, 
         onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE IF NOT EXISTS clinica ("
              "id TEXT PRIMARY KEY,"
              "nome TEXT,"
              "latitude numeric(20,17),"
              "longitude numeric(20,17),"
              "sync integer"
              ")");
    });
  }
}
