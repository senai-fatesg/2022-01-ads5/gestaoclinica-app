import 'package:gestaoclinica_app/providers/db_providers.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

class Clinica {
  String id = Uuid().v4();
  String? nome;
  double? latitude;
  double? longitude;
  int sync = 0;

  Clinica({
    required this.id,
    this.nome,
    this.latitude,
    this.longitude,
    required this.sync,
  });  


  factory Clinica.fromMap(Map<String, dynamic> json) => Clinica(
        id: json["id"],
        nome: json["nome"],
        latitude: json["longitude"],
        longitude: json["latitude"],
        sync: json["sync"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "nome": nome,
        "latitude": latitude,
        "longitude": longitude,
        "sync": sync,
      };  
}

class ClinicaHelper {
  static final ClinicaHelper _instance = ClinicaHelper.internal();

  factory ClinicaHelper() => _instance;

  ClinicaHelper.internal();

  

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
    }

 Future<void> insert(Clinica clinica) async {
    // Get a reference to the database.
    final db = await DBProvider.db.database;

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db?.insert('clinica', clinica.toMap());
  }    

   Future<List<Clinica>> listar() async {
    final db = await DBProvider.db.database;
    var res = await db?.query("clinica");
    List<Clinica> list = res!.isNotEmpty ? res.map((c) => Clinica.fromMap(c)).toList() : [];
    return list;
  }  
}