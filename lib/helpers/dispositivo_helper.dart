import 'dart:convert';
import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class DispositivoHelper {
  static final DispositivoHelper _instance = DispositivoHelper.internal();
  factory DispositivoHelper() => _instance;
  DispositivoHelper.internal();

  bool _configuracaoIniciada = false;

  bool _ativado = false;

  String? _id;



Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

  Future<File> get _configurationFile async {
    String dir = await _localPath;
    print(dir);
    return File('$dir/dispositivo.json');
  }

  Future<Map<String, dynamic>> get _configurationMap async {
    File file = await _configurationFile;
    final contents = await file.readAsString();
    Map<String, dynamic> data = json.decode(contents);
    return data;
  }

  bool isConfiguracaoIniciada() {
    return _configuracaoIniciada;
  }

  Future<bool> isDispositivoAtivado() async {
    if (_ativado) {
      print('ja esta ativo');
      return _ativado;
    } else {
      print('verificando ativacao');
      File file = await _configurationFile;
      if (await file.exists()) {
        Map<String, dynamic> data = await _configurationMap;
        _configuracaoIniciada = true;
        _ativado = data['ativo'];
        return _ativado;
      } else {
        return false;
      }
    }
  }

  Future<void> configurar(String nome, String telefone) async {

    final deviceInfoPlugin = DeviceInfoPlugin();
    final deviceInfo = await deviceInfoPlugin.deviceInfo;
    final map = deviceInfo.toMap();
    print('config');
    print(map);
    print('config');
    String jsonText = jsonEncode(<String, dynamic>{
        'telefone': telefone,
        'nome': nome,
        'deviceInfo': map,
      });
      print(jsonText);

    final response = await http.post(
      Uri.parse(
          'https://gestaoclinica.acesso.ws/gestaoclinica-ws/rest/dispositivo'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonText,
    );

    if (response.statusCode == 200) {
      _id = response.body;
      Map<String, dynamic> map = {'id': _id, 'telefone': telefone, 'ativo': false};
      final content = jsonEncode(map);
      File file = await _configurationFile;
      file.writeAsString(content);
      _configuracaoIniciada = true;
      print('configuracao iniciada');
    } else {

    }

    // _configurado = true;
  }

  Future<void> ativar(String codigoAtivacao) async {
    print('ativando');

    if(_id == null){
      Map<String, dynamic> map = await _configurationMap;
      _id = map['id'];
    }

    final response = await http.get(
      Uri.parse(
          'https://gestaoclinica.acesso.ws/gestaoclinica-ws/rest/dispositivo/ativar/$_id/$codigoAtivacao'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    print('ativando');

    if (response.statusCode == 200) {
      Map<String, dynamic> resp = json.decode(response.body);
      String status = resp['status'];
      if(status == 'success'){
        Map<String, dynamic> user = resp['user'];
        Map<String, dynamic> config = await _configurationMap;        
        config['ativo'] = true;
        config['codigoAtivacao'] = codigoAtivacao;
        config['wsLogin'] = user['wsLogin'];
        config['wsPass'] = user['wsPass'];
        final content = jsonEncode(config);
        File file = await _configurationFile;
        file.writeAsString(content);
        _ativado = true;
      }
    } else {
      print(response.statusCode);
      print(response.body);
    }

    // _configurado = true;
  }

}

