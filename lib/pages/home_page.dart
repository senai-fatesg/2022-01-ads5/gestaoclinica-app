import 'package:flutter/material.dart';
import 'package:gestaoclinica_app/helpers/dispositivo_helper.dart';
import 'package:gestaoclinica_app/pages/clinica_page.dart';

import 'confirmacao_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  bool _emAtivacao = false;

  @override
  void initState() {
    super.initState();
  }

  verificarAtivacao() async {
    print('verificar ativacao');
    DispositivoHelper dispositivoHelper = DispositivoHelper();
    _emAtivacao = !await dispositivoHelper.isDispositivoAtivado();
    if (_emAtivacao) {
      print('nao esta ativado');
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ConfirmacaoPage()),
      ).then((value) => setState(() {_emAtivacao = false;}));
    }else{
      print("nao fazer nada... disp ativo");
    }
  }

  @override
  Widget build(BuildContext context) {
    verificarAtivacao();
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ClinicaPage()),
                  ).then((value) => setState(() {

                  }));

                }, 
                child: Text('Clínicas')),
            ],
          ),
        ),
      ),
    );
  }
}
