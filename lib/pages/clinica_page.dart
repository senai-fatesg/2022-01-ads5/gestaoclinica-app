import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:uuid/uuid.dart';

import '../helpers/clinica_helper.dart';

class ClinicaPage extends StatefulWidget {
  const ClinicaPage({Key? key}) : super(key: key);

  @override
  _ClinicaPageState createState() => _ClinicaPageState();
}

class _ClinicaPageState extends State<ClinicaPage> {
  TextEditingController nomeClinicaController = TextEditingController();

  TextEditingController locationController = TextEditingController();

  double? _latitude;

  double? _longitude;

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  @override
  void initState() {
    super.initState();
    _determinePosition().then((value) => setCoordenadas(value));
  }

  void setCoordenadas(Position pos) {
    _latitude = pos.latitude;
    _longitude = pos.longitude;
    locationController.text = '$_latitude; $_longitude';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.fromLTRB(16, 32, 16, 0),
        child: Column(
          children: [
            TextField(
              controller: nomeClinicaController,
              decoration: InputDecoration(
                labelText: 'Nome da Clínica',
              ),
            ), 
            TextField(
              enabled: false,
              controller: locationController,
              decoration: InputDecoration(
                labelText: 'Localização',
              ),
            ),
            ElevatedButton(
                onPressed: gravarClinica,
                child: Text('Configurar'),
            )
          ],
        ),
      ),
    );
  }

  void gravarClinica(){
    Clinica clinica = Clinica(id: Uuid().v4(), nome: nomeClinicaController.text, latitude: _latitude, longitude: _longitude, sync: 0);
    ClinicaHelper().insert(clinica);
    print('clinica salva');
  }
}
