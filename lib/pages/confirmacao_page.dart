import 'package:flutter/material.dart';
import 'package:gestaoclinica_app/helpers/dispositivo_helper.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';


class ConfirmacaoPage extends StatefulWidget {
  const ConfirmacaoPage({Key? key}) : super(key: key);

  @override
  _ConfirmacaoPageState createState() => _ConfirmacaoPageState();
}

class _ConfirmacaoPageState extends State<ConfirmacaoPage> {

  TextEditingController nomeController = TextEditingController();
  TextEditingController telefoneController = TextEditingController();
  TextEditingController ativacaoController = TextEditingController();

  String? mensagemErroNome;
  String? mensagemErroTelefone;
  String? mensagemErroAtivacao;

  bool configuracaoIniciada = false;

  @override
  Widget build(BuildContext context) {
    DispositivoHelper dispositivoHelper = DispositivoHelper();
    configuracaoIniciada = dispositivoHelper.isConfiguracaoIniciada();
    return Scaffold(
      body: configuracaoIniciada ? getAtivacao() : getConfiguracao(),
    );
  }

  Widget getAtivacao(){
    return Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: ativacaoController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Código de Ativação',
                  errorText: mensagemErroAtivacao,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  ativar(context);
                },
                child: Text('Ativar'),
              ),
            ],
          ),
        ),
      );
    
  }


  Widget getConfiguracao(){
    var maskFormatter = new MaskTextInputFormatter(
        mask: '(##) # ####-####', filter: {"#": RegExp(r'[0-9]')});


    return Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: nomeController,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  labelText: 'Nome',
                  errorText: mensagemErroNome,
                ),
              ),
              TextField(
                controller: telefoneController,
                inputFormatters: [maskFormatter],
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Telefone',
                  errorText: mensagemErroTelefone,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  configurar(context);
                },
                child: Text('Ativar'),
              ),
            ],
          ),
        ),
      );
    
  }

  void ativar(BuildContext context) async{
    String codigoAtivacao = ativacaoController.text;
    DispositivoHelper dispositivoHelper = DispositivoHelper();
    dispositivoHelper.ativar(codigoAtivacao).then((value) => Navigator.pop(context, true));
  }

  void configurar(BuildContext context) async{
    String telefone = telefoneController.text;
    telefone = telefone.replaceAll(RegExp('[^0-9]'), '' );
    String nome = nomeController.text;
    bool valido = true;

    setState((){
      mensagemErroTelefone = null;
      mensagemErroNome = null;
    });

    if(telefone.length != 11){
      valido = false;
      setState(() {
        mensagemErroTelefone = "Informe o telefone de forma correta";
      });
    }
    
    if(nome.length < 6){
      valido = false;
      setState(() {
        mensagemErroNome = "Qual o seu nome?";
      });
    }
    
    if(valido){
      print('clicou $telefone');
      DispositivoHelper dispositivoHelper = DispositivoHelper();
      dispositivoHelper.configurar(nome, telefone).then((value) => {setState( () {telefoneController.clear();} )});

      print('configurado');
    }
  }


}