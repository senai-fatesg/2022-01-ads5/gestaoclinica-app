import 'package:flutter/material.dart';
import 'package:gestaoclinica_app/pages/confirmacao_page.dart';

class TestePage extends StatefulWidget {
  const TestePage({Key? key}) : super(key: key);

  @override
  _TestePageState createState() => _TestePageState();
}

class _TestePageState extends State<TestePage> {

  @override
  void initState(){
    super.initState();
  verificar();
  }

  void verificar() async {
    await Future.delayed(const Duration(seconds: 2), () => print('Large Latte'));

    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ConfirmacaoPage()),
      ).then((value) => setState(() {}));    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}